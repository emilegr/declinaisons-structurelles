# Lev Manovich - Database As A Symbolic Form

## The Database Logic

analyse / comparaison des formes d'expressions culturelles :
- récit (priviligié par le roman, cinéma, etc.)
- base de données (éléments individuels chaque éléments a la même valeur que les
  autres)

base de donnée (informatique): collection structurée de données, les données sont organisées
pour une recherche rapide et extraction par un ordinateur.

types de base de données :
- relationelles
- ...

base de données apparaît comme une collection d'éléments avec laquelle
l'utilisateur peut effectuer certaines opérations : voir, naviguer, chercher
(utilisation distincte de la lecture d'un récit, du visionnement d'un film ou de
la navigation dans un site architectural).

> the world appears to us as an endless and unstructured collection of images, texts, and
> other data records, it is only appropriate that we will be moved to model it as a database.
> But it is also appropriate that we would want to develops poetics, aesthetics, and ethics of
> this database.

- musée virtuel : embarque l'utilisateur pour un "tour" de la collection. base de
donnée d'images qui peut être accéder de différentes façon : chronologique, par
pays, par artiste. (récit devient juste une methode d'accès aux données)
- page web : liste sequentielle d'éléments séparés. toujours possible d'ajouter un
nouvel éléments à cette liste.

> If new elements are being added over time, the result is a collection, not a
> story. Indeed, how can one keep a coherent narrative or any other development
> trajectory through the material if it keeps changing?

## Data and Algorithm

jeux-vidéos vécu comme des récits car il y a une tâche à accomplir.

[...]

programmation informatique :
- data structures
- algorithms

> The more complex the data structure of a computer program, the simpler the algorithm needs to be, and vice versa.

