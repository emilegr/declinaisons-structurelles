
<?php

    $value = file_get_contents('php://input');
    $data = json_decode(file_get_contents('data.json'), true);
    if (in_array($value, $data)) {
        echo 'already exists';
    } else {
        array_push($data, $value);
        if (file_put_contents('data.json', json_encode($data, JSON_PRETTY_PRINT))) {
            echo 'ok';
        } else {
            echo 'not ok';
        }
    }
