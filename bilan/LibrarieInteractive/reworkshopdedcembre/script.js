window.addEventListener("load", setup);
function setup(){
    
    console.log("setup");
    loadJSON("codebeautify.json", handleJSON);
    //appel du code json et de la function handleJSON() 
    
    
    var contenairecause = document.getElementById("causeboite");
    var contenaireeffet = document.getElementById("effetboite");
    var btnsCause = contenairecause.getElementsByClassName("cause");
    var btnsEffet = contenaireeffet.getElementsByClassName("effet");
    
    for (var i = 0; i < btnsCause.length; i++) {
        btnsCause[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("cause active");
            if (current.length > 0) { 
                current[0].className = current[0].className.replace(" active", "");
            }
            this.className += " active";
        });
    }
    for (var i = 0; i < btnsEffet.length; i++) {
        btnsEffet[i].addEventListener("click", function() {
            var currentEffet = document.getElementsByClassName("effet active");
            if (currentEffet.length > 0) { 
                currentEffet[0].className = currentEffet[0].className.replace(" active", "");
            }
            this.className += " active";
        });
    }
    
    
    
}


var mainJson;
// var gofunction;
// var elm = document.getElementsByTagName("div");
var cumulatedFunction; 


function handleJSON(json){
    mainJson = json;
    //console.log(mainJson.interactivity.cause.computer.mouseEvent.onclick);
    
    
    document.getElementById("ok").addEventListener("click", function(){
        var contenairecause = document.getElementById("causeboite");
        var contenaireeffet = document.getElementById("effetboite");
        var current = contenairecause.getElementsByClassName("active")[0];
        var currentEffet = contenaireeffet.getElementsByClassName("active")[0];
        var DataCause = current.getAttribute("data-json");
        var DataEffet = currentEffet.getAttribute("data-json");
        // var cause=mainJson.DataCause;
        // var effet=mainJson.DataEffet;
        // var DataCause = document.getElementById('click').getAttribute("data-json");
        // var DataEffet = document.getElementById('rotate').getAttribute("data-json");
        //var cumulatedFunction = convertToFunction(mainJson.interactivity.cause.computer.mouseEvent.onclick + mainJson.interactivity.effect.opacity);
        var cumulatedFunction = convertToFunction(DataCause+DataEffet);
        cumulatedFunction(); 

        var txt = DataCause+DataEffet;
        // var re = /;/gi;
        // var refunction = /{/gi;
        // var nvtxt = txt.replace(re, ';<br>');
        // var nvtxt_2 = nvtxt.replace(refunction, '{<br>');

        //  var nvtxt = txt.replace(";", ";<br>");
        // var nvtxt_2 = nvtxt.replace("{", "{<br>");

        var nvtxt = txt.replace(/;/gi, ";<br>");
        var nvtxt_2 = nvtxt.replace(/{/gi, "{<br>");
        console.log(nvtxt_2);

        document.getElementById("code").innerHTML=nvtxt_2;

        var div_result = document.querySelector("#Result");
        var collection_div_result = div_result.querySelectorAll("div");
        var last_div_result = collection_div_result[collection_div_result.length - 1];
        
        last_div_result.addEventListener("mouseover",updateStyle);
            /*
        var cube=this;
            var rotate = window.getComputedStyle(cube).getPropertyValue('transform');
            var opacity = window.getComputedStyle(cube).getPropertyValue('opacity');
            console.log('rotate('+rotate+'deg)<br> opacity:'+opacity);
            var info= '<br><br>rotate('+rotate+'deg)<br> opacity:'+opacity;
            document.getElementById("code").innerHTML=nvtxt_2+info;

        });
        */




    });
    

}

function updateStyle() {
    var cube=this;
    var txtcode= document.getElementById('code');
            var rotate = window.getComputedStyle(cube).getPropertyValue('transform');
            var opacity = window.getComputedStyle(cube).getPropertyValue('opacity');
            console.log('rotate('+rotate+'deg)<br> opacity:'+opacity);
            var info= '<br><br>rotate('+rotate+'deg)<br> opacity:'+opacity;
            document.getElementById("code").innerHTML+=info;

}
function convertToFunction(string){
    var newFunction = Function(string);
    return newFunction;
}


function loadJSON(url, callback){
    
    var xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = function() {
        
        if (this.readyState == 4 ) {
            
            if(this.status == 200 || this.status == 0){
                var json = JSON.parse(xhttp.responseText);
                callback(json);
            }
        }
    };
    
    xhttp.open("GET", url, true);
    xhttp.send();
}