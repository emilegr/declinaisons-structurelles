# Format de documents : Déclinaisons structurelles (back end) 

*Vincent Maillard et Émile Greis*

Expérimentations et interrogations sur les systèmes d'inscription, d'organisation et de description de contenus. Allers et retours dans la fiction, la spéculation, l'histoire et la technique. Notions d'arborescence, de relations, d'interopérabilité, et d'incidences des formats sur la gestion des contenus. En bref, réflexions communes aux collectionneurs, archivistes, bibliothécaires, philatélistes, lépidoptéristes ou autres énumérophiles. 

http://esad-gv.net/designgraphique/_evenements/2018_Espace_editorial/

Git outils d'inscription, de versions et partage de données.

Paul Otlet (1868 -1944) : classifications dans les bibliothèques: fiches de classifications avec différentes entrée (auteur, titre...)
 Livre: Traité de documentation, le livre sur le livre

Mondothèque : Bureau multimédia  1936
Mundaneum
Classification de Dewey: classification décimale => Paul Otlet s'en ai inspiré (que l'on retrouve notamment dans les bibliothèques )
 rattrapé par Google qui a voulu y voir l'ancêtre des moteurs de recherche (vers 2015)
 
 Au début du web: classifications des sites web par type, arborescence (art, sport...)
 PageRank : créer en essayant d'apporter de l'objectivité dans la classifications.
 Analyse de la structure de liens du web, entre l'hypertextualité des pages Web -> plus il y a de liens (trafic) vers une page, plus elle a de valeur.
 Même système dans les publications scientifiques, notamment avec les citations d'autres publications. Plus notre texte est cité plus il est reconnu, importance aussi des sources que l'on cite.
 Elsevier : site de publication scientifique payant
 Sci-Hub : site permettant d'avoir accès aux publication scientifique notamment disponible sur Elsevier, le site met redirige le lecteur vers un site d'un école ou autre, qui on accès au texte que l'on veux lire.
 
 Astrom/Zimmer 
 http://www.sitterwerk-katalog.ch/
 Réalisé à partir d'une collections personnel (livres, objets...)


http://p-dpa.net/
Site de classification pour garder en mémoire des expérimentations, livres... Le site propose différentes entrées.
 
 SPARQL : langage permettant à travers une requête de rechercher, d'ajouter, de modifier ou de supprimer des données disponible sur internet.
 http://data.persee.fr/sparql
 
 Collection du musée Van Gogh à Amsterdam : https://www.vangoghmuseum.nl/en/explore-the-collection
 
 Louise Drulhe questionnement visuel autour des questions qu'es ce que c'est internet? Traduction experimental, basé sur l'expérience d'internet (pas le côté technique plus les sensations)
 http://internet-atlas.net/

## outils

### terminal basics (utiliser tabulation pour autocompletion)

créer un dossier

mkdir nom-du-dossier

se déplacer dans un dossier

cd nom-du-dossier

remonter d'un dossier

cd ..



### requête wget

wget -nd -H -r -A gif, http://esad-gv.net/designgraphique/DG4/Victor_Calame/

version qui marche
wget -e robots=off -nd --no-parent -r -A rtf http://www.esad-gv.fr/designgraphique/DG4/Julie_Moreau/_wget_test/

-nd prevents the creation of a directory hierarchy (i.e. no directories).
-r enables recursive retrieval. See Recursive Download for more information.
-A sets a whitelist for retrieving only certain file types. Strings and patterns are accepted, and both can be used in a comma separated list (as seen above). See Types of Files for more information.
-H: span hosts (wget doesn't download files from different domains or subdomains by default)
- noparent ???

### langages

Markdown : mise en page par l'utilisation de code (exemple : **gras**). Écriture plus simple, peut être convertis en HTML.
https://stackedit.io/app#

CSV : permet à partir d'un texte de créer un tableau à partir de données séparées par des virgules (Comma-separated values)

JSON : format de hiérarchisation type arborescence 
{ "clef" : "valeur" {sous classification toujours en "clef" : "valeur" } }
Compatible avec Javascript

## Pistes

- repérage visuel fichier web server / cartographie, organisation, hierarchisation, arborescence du server, route unique
- organisation temporelle du serveur
- photos prise par didier (archives de l'école) voir avec claire 
- plateforme d'échange de travaux en cours
- collection d'interactions
- extraire des contenus du serveur 

(fermé le serveur, mettre des mots de passe, protégé ?)
accéder aux sites perso sur serveur

un simple index.html "bloque" l'accès ?
http://www.esad-gv.fr/designgraphique/_archives_etudiants/DNSEP_2016/Benjamin_Dumond/


## Les groupes

- repérage visuel fichier web server / cartographie, organisation, hierarchisation, arborescence du server, route unique
 Louise, Adèle
- organisation temporelle du serveur
- photos prise par didier (archives de l'école) voir avec claire 
Guillaume, Thomas, Chloé,Lorène ,terava,raph
- plateforme d'échange de travaux en cours
- collection d'interactions
margot
- extraire des contenus du serveur
Julie, Célia, Adèle
- timestamp







### archives photos

ARCHIVES PHOTOS A LA CHAMBRE LIEU-TERRITOIRE
 
Récupérer toutes les archives de photos à la chambre prises avec Gilles. 
Lorsqu’un même lieu est choisi d’une année à l’autre par des différents élèves, idée de pouvoir comparer le point de vue choisi/l’évolution physique du lieu… 
=> une manière de traiter question des doublons (ou plus) = chercher le liant entre ces photos (les commentaires de Didier ?qui se souvient de certains lieux/certaines photos des années passées) = présence d'une trace écrite aussi.

Comment classer les archives numériques : date (année, promo), format de document (.TIF, .JPEG… conserver le fichier le plus lourd pour pouvoir zommer en détail ? Mais pb du poids ?), nom de l’élève, nom du lieu, type de lieu (commerces, patrimoine, privé…), type de photo (paysage, détail) ?
Comment montrer ces photographies : les identifier (par leur nom d’auteur/le nom du lieu ?), créer un id pour ces photos
Idée : carte de Valence => classer les photos en fonction de leur position sur la carte, interactivité.

Une question qui se pose aussi du point de vue matériel /physique : archivage des films.
Rapport avec les archives de la ville (voir avec Gilles)
Relier chaque photo à des données textuelles = notice descriptive / interview ? Penser/repérer un système/une structure 
Classeur de Didier = comment sont classés les négatifs = laisser une trace de la notion matérielle du film sur l'interface (photo des négatifs par exemple) = relier différents types de docs. 
 

### extraction serveur

- outils:
  * wget
  * curl

difference between curl and wget

https://unix.stackexchange.com/questions/47434/what-is-the-difference-between-curl-and-wget

#### installer wget sur mac OS

d'abord installer homebrew https://brew.sh/index_fr

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

puis wget

brew install wget

####  wget tuto

https://chris.partridge.tech/data/wget-noobs.pdf
http://wget.addictivecode.org/Faq.html 

version qui marche sur le serveur de l'école et les fichier Robots.txt ignorés!
wget -e robots=off -nd --no-parent -r -A rtf http://www.esad-gv.fr/designgraphique/DG4/Julie_Moreau/_wget_test/


# Deuxième jour

## Tour de table pour résumer les envies, les perspectives pour la suite

Archives photo (Terava, Lorène, Chloé) :
    - lieu territoire - photos à la chambre depuis 2012
    - Notice descriptive : lieu, doublons (et plus)
    - Archives matériels
    
Louise, Adèle, Julie :
    - extraire du serveur => quel espace, quel classification ?
    - espace intermédiaire entre le terminal (wget)

Margot :
    - classification JSON => mise en ordre d'idées



## mini-point autour de la recherche :

La recherche semble assez loin / abstraite pour les étudiants en DG2. et DG3 !
Se raccrocher à des choses plus proche plus concrète (territoire)
Découverte récente du site enjeuxdudesigngraphique

L'unité de recherche à des lignes de recherche
- Archéologie des médias
- Représentation de l'histoire non linéaire / du temps / récits
- Programmation visuelle / Langages de programmation
- ...

différence entre labo de recherche / unité de recherche


##  LE PAD MUSIQUE

mettez ici vos plus belles chansons : 🤘

https://mypads.framapad.org/p/musique-zg14pk7yj?&auth_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6IkFkbWluRWRpdG8iLCJrZXkiOiJjanBheHY3dHcwMTRva3hyN3VpaW45MW5oIiwiaWF0IjoxNTQ0MDAwMjIxfQ.P5QMQuJHUr1HRAA0RvqJyu4EjBb8Tk4mdNpxkrnMkFw&userName=AdminEdito


## random server bot

- quel type de fichiers ?
- restrictions ?
- quel temporalité ? chaque jour, sur événement (quelqu'un sur serveur)
- methodes:
    * télécharger ensemble gifs puis choir un rdm each morning
    * se balader dans le serveur de manière aléatoire
- récupérer mailing comptes esad
- donner la source du gif ou non ?


envoyer un mail gmail/python

https://stackoverflow.com/questions/10147455/how-to-send-an-email-with-gmail-as-provider-using-python
#!/usr/bin/env python2.7

import smtplib
fromaddr = 'user_me@gmail.com'
toaddrs  = 'user_you@gmail.com'
msg = 'Why,Oh why!'
username = 'user_me@gmail.com'
password = 'pwd'
server = smtplib.SMTP('smtp.gmail.com:587')
server.ehlo()
server.starttls()
server.login(username,password)
server.sendmail(fromaddr, toaddrs, msg)
server.quit()

en fait plustôt maintenant google API
https://developers.google.com/gmail/api/quickstart/python

suite troisième jour...

### post.php

<?php

    $value = file_get_contents('php://input');
    $data = json_decode(file_get_contents('data.json'), true);
    if (in_array($value, $data)) {
        echo 'already exists';
    } else {
        array_push($data, $value);
        if (file_put_contents('data.json', json_encode($data, JSON_PRETTY_PRINT))) {
            echo 'ok';
        } else {
            echo 'not ok';
        }
    }

# Troisième jour

Vendredi aprem : restitution
Quelle tournure ça va prendre ? Quelle mise en commun entre les trois workshops ?

Au sujet de l'unité de recherche : http://enjeuxdudesigngraphique.fr/
Présentation des axes de recherche, des différentes publications (.txt, From:To)


## php mail pour joyeux noel

$to      = 'nobody@example.com';
$subject = 'the subject';
$message = 'hello';
$headers = 'From: webmaster@example.com' . "\r\n" .
    'Reply-To: webmaster@example.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);




$message = "<html><head>
<title>Your email at the time</title>
</head>
<body>
<img src=\"http://www.myserver.com/images_folder/my_image.jpg\">
</body>"


function array_flatten($array) { 
  if (!is_array($array)) { 
    return FALSE; 
  } 
  $result = array();
    
  foreach ($array as $key => $value) { 
    if (is_array($value)) { 
      $result = array_merge($result, array_flatten($value)); 
    } 
    else { 
      $result[$key] = $value; 
    } 
  } 
  return $result;
 } 




'MIME-Version: 1.0';
'Content-type: text/html; charset=iso-8859-1'



function get-files-paths($dir) {
    $root = scandir($dir);
    foreach($root as $value)
    {
        if($value === '.' || $value === '..') {continue;}
        if(is_file("$dir/$value")) {$result[]="$dir/$value";continue;}
        foreach(find_all_files("$dir/$value") as $value)
        {
            $result[]=$value;
        }
    }
    return $result;
}

$paths = get-files-paths('..');


# functions

function find_all_files($dir) {
    $root = scandir($dir);
    foreach($root as $value)
    {
        if($value === '.' || $value === '..') {continue;}
        if(is_file("$dir/$value")) {$result[]="$dir/$value";continue;}
        foreach(find_all_files("$dir/$value") as $value)
        {
            $result[]=$value;
        }
    }
    return $result;
}

function filter_by_extension($files, $extensions) {
    $result = [];
    foreach ($extensions as $extension) {
        #print($extension);
        foreach ($files as $file) {
            #print(pathinfo($file, PATHINFO_EXTENSION));
            if ($extension === pathinfo($file, PATHINFO_EXTENSION)) {
                array_push($result, $file);
            }
        }
    }
    return $result;
}

# config

$targets = [
    '..'
];

$extensions = [
    'gif'
];

# init

$selected = [];

foreach ($targets as $target) {
    $files = find_all_files($target);
    $filtered = filter_by_extension($files, $extensions);
    $selected = array_merge($selected, $filtered);

}

#print_r($selected);
echo $selected[array_rand($selected)];


Liens framapad page web insolite de l'index:
    https://mensuel.framapad.org/p/ve4VLgZfmR 
    merci


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title></title>
        <script>
            fetch('https://mensuel.framapad.org/p/ve4VLgZfmR/export/txt')
                .then(function(response) {
                    return response.text();
                })
                .then(function(text) {
                    var lines = text.split('\n');
                    console.log(lines);
                });
        </script>
    </head>
    <body>
        <button id="lol">oui</button>
    </body>
</html>





