Quelques notes...

TEMPLATE :

## nom
url
description et commentaire




--------------------------------------------------

# FOLKSONOMIE / VOCABULAIRES
### Dublin Core
http://www.bnf.fr/fr/professionnels/formats_catalogage/a.f_dublin_core.html
Le Dublin Core est un format descriptif simple et générique, comprenant 15 éléments.

## FOAF (Friend Of A Friend)
http://xmlns.com/foaf/spec/

## Bibliographic Ontology
http://bibliographic-ontology.org/

## Protocole OAI-PMH (basé sur le Dublin Core)
Pour permettre à d’échanger des métadonnées par institutions
http://www.bnf.fr/documents/Guide_oaipmh.pdf

## Liste de vocabulaires
https://lov.linkeddata.es/dataset/lov/vocabs

--------------------------------------------------

# OUTILS

## Omeka
https://omeka.org/

## Zotero
https://www.zotero.org/

## Open Archives Initiative - Repository Explorer
http://re.cs.uct.ac.za/


--------------------------------------------------

# VIDÉOS

## Alain Resnais, « Toute la mémoire du monde », 1956
 https://www.youtube.com/watch?v=i0RVSZ_yDjs

## Francis Hunger, Deep Love Algorithm
https://vimeo.com/74108998

--------------------------------------------------

# CAS PRATIQUES

## Darwin - Variorum
http://darwin-online.org.uk/Variorum/1861/1861-1-dns.html

## Darwin - Traces
 https://fathom.info/traces/ 

## AIME
http://modesofexistence.org/

## Arpentage
http://la-trouvaille.org/arpentage/#comments


--------------------------------------------------

# API

## Harvard Library APIs & Datasets
https://library.harvard.edu/services-tools/harvard-library-apis-datasets

aaron swartz livre a programmable web an unfinished work


--------------------------------------------------

# PROJETS

## Library Genesis
http://gen.lib.rus.ec/

## Aaaaarg
http://aaaaarg.fail/
Une bibliothèque en ligne assez fournie.  La navigation dans un document scanné est impressionnante : http://aaaaarg.fail/ref/3e65e3e37bdb4af891cd2871e1fe90a8

## Craigslist
https://paris.aigslist.fr/

## Bibliotheca
http://bibliotecha.info/
Le projet Bibliotheca dont je te parlais. Un framework pour la distribution de fichiers, dans des réseaux locaux, "pirate box" ou équivalent.

## Interstices
http://interstices.io/
Le carnet de bord d'Alexandre Liziard et d'Étienne Ozeray pour leur projet de diplôme.

## Archi-Trace
http://antoine-n.fr/archi-trace/

## Canal U
http://www.canal-u.tv/
La vidéothèque numérique de l'enseignement supérieur. Une web tv pour les enseignants et les étudiants.

## Arte Radio
http://arteradio.com/categorie/documentaire
Accès aux fichiers sonores de Arte Radio

## Internet Archive
https://archive.org/
a non-profit library of millions of free books, movies, software, music, and more.

## Post-digital publishing archive
http://p-dpa.net/

## Delectable
http://www.ecogex.com/delectable/
Sélection de sites par le studio Ecogex (Nathaniel Hubert et Malik Diouri) (C'est Raphael plutot non ?)
(http://crapisgood.com/interview-ecogexdelectable/)

## Monoskop 
http://monoskop.org/Monoskop
Welcome to Monoskop, a wiki for collaborative studies of the arts, media and humanities.  

## Traumawien
http://traumawien.at/

## Rhizome artbase
https://rhizome.org/art/artbase/
Archives d'œuvres numériques

## Roma publications
http://www.romapublications.org/Roma151-300.html
Voir aussi : http://www.orderromapublications.org/publications Independent art publisher. A platform to produce autonomous publications in close collaboration with a growing number of artists, institutions, writers and designers. Related to the content, every issue has its own rule of appearance and distribution, varying from house to house papers to exclusive books. Until now the publications have editions between 2 and 150,000

## The serving library
http://www.servinglibrary.org/
Dexter Sinister

## Memory of the world
https://www.memoryoftheworld.org/
Marcel Mars, un partage de fichier en peer to peer

## Catalog tree
http://www.catalogtree.net/

## Arxiv
http://arxiv.org/
Open access to 1,117,136 e-prints in Physics, Mathematics, Computer Science, Quantitative Biology, Quantitative Finance and Statistics

## Ubu Web
http://www.ubuweb.com/

## Sci-hub
https://sci-hub.io/

## Framabook
http://framabook.org/

## Open library
https://openlibrary.org/

## Digital Public Library of America
https://dp.la/

## Open Archives Institute
http://www.openarchives.org/

## Collecta 
https://www.collecta.fr/
Je pense que ça vaut le coup de passer le moment où Anthony Masure en parle dans cette vidéo (de 11'30 à 19'50):
https://www.youtube.com/watch?v=e9iTMgZqupg

--------------------------------------------------



# RESSOURCES THÉORIQUES

## Active Archives manifesto
http://activearchives.org/wiki/Manifesto_for_an_Active_Archive
super cool

## Archiving Is The New Folk Art
http://www.poetryfoundation.org/harriet/2011/04/archiving-is-the-new-folk-art/

## Digital preservation practices and the Rhizome ArtBase
http://media.rhizome.org/artbase/documents/Digital-Preservation-Practices-and-the-Rhizome-ArtBase.pdf
article par rapport aux problèmes et stratégies d'archives d'œuvres d'art numérique

## Institute of Network culture
http://networkcultures.org/
Institut de recherches basé à Amsterdam, em lien avec le publishing lab

## Bibliogifts in LibGen? A study of a text-sharing platformdriven by biblioleaks and crowdsourcing
ftp://www.irit.fr/IRIT/SIG/2015_JASIST_C.pdf

## Database cultures
http://databasecultures.irmielin.org

## Lev Manovich, Database as Symbolic Form :
http://journals.sagepub.com/doi/abs/10.1177/135485659900500206?journalCode=cona

## Bibliographie matérielle
http://dominique-varry.enssib.fr/node/31

## Outils bibliographiques du bibliographe
http://dominique-varry.enssib.fr/node/42

##  Manuel de constitution des bibliothèques numériques 
http://www.bnf.fr/fr/collections_et_services/anx_biblios_livre/a.biblio_manuel_numerique.html
Préface : https://www.bruno-ory-lavollee.eu/images/2018/05/Pr%C3%A9face-Manuel-de-constitution-de-biblioth%C3%A8ques-num%C3%A9riques.pdf

## Isabelle Westeel, Indexer, structurer, échanger : métadonnées et interopérabilité
https://books.openedition.org/pressesenssib/434#text

## How To Build A Digital Library
http://what-when-how.com/category/how-to-build-a-digital-library/

## Statistiques demandes dans la collection d’une bibliothèque :
http://bbf.enssib.fr/consulter/bbf-2002-05-0124-010
C. Anderson / The long tail : http://www.longtail.com/about.html

## Jean-Michel Salaünn, Vu, lu, su : les architectes de l'information face à l'oligopole du Web
Vision du web en héritage des modèles de la classification des documents (Mundaneum - P. Otlet, Memex - V. Bush)
