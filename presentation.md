# Déclinaisons structurelles

organisation et l'inscription (ajout) des contenus
qu'ils soient textuels, images, vidéos,

annexes
articulations 


sauvegarde pour rendre accessible

## programme

- introduction
- présentations + tools
- pratiques / attentes
- dans le cadre de plateforme édition ?

## databases ?

> What is a database? any systematic attemps to keep track of things.

Ted Nelson, Computers for Cynics 3 - The Database Mess, 2012, <https://www.youtube.com/watch?v=bhzD2FKEEds>

> In computer science database is defined as a structured collection of data.
> The data stored in a database is organized for fast search and retrieval by a
> computer and therefore it is anything but a simple collection of items.

Lev Manovich, Database As A Symbolic Form, 1998

## Dimensions

- prospectif / spéculative 
  * Internet Atlas, Louise Druhle <http://internet-atlas.net/>
  * Deep Love Algorithm, Francis Hunger <https://vimeo.com/74108998>
- expérimentale 
- fonctionelle
- théorique
- ... ?

## ?

- typos
- pdfs
- ressources
- bibliotheque perso
- bibliotheque communs
- liens
- actu
- commentaires
- notes
- edt / agenda /calendrier

## Mail Dominique

- Conférences filmées, mais sans les vidéos, il s'agit des documents annexes (présentation pdf)
- Mémoires DNSEP (liste de tous les mémoires + titres/auteur, scans des tables des matières?) : dépouillement de mémoires.
- PDF des mémoires DG5
- Archives from-to (le site, les articles).
- Invités annuels : Listes des sujets et visibilité des réalisations.

Consultables ici : http://esad-gv.net/designgraphique/_evenements/2018_Espace_editorial/corpus_ressources/

voici les documents utiles :

https://docs.google.com/document/d/1bzTlQROZivSmQWT3B7x_IpUaVj7U249ImbgOEWGXjKE/edit#

https://docs.google.com/document/d/11M550fetIf1jent7tNRCuHK4_okVtmTaUcW_s8VTLJ4/edit#

LA LISTE DES INSCRITS!!

https://mypads.framapad.org/mypads/?/mypads/group/semaine-espace-editoriale-962ckn71t/pad/view/inscriptions-aux-workshops-i35sf70j

## Pads

https://mypads.framapad.org/mypads/?/mypads/group/semaine-espace-editoriale-962ckn71t/view
