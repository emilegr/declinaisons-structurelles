# tools

## langages / formats

markup
- html
- markdown <https://en.wikipedia.org/wiki/Markdown#Example>
- xml

data serialization
- json <https://en.wikipedia.org/wiki/JSON#Comparison_with_other_formats>
- yaml

markdown vers html avec pandoc

    pandoc test.md -o test.html

markdown vers html avec parsedown

    include 'Parsedown.php';
    $Parsedown = new Parsedown();
    $content = file_get_contents('test.md');
    $html = $Parsedown->text($content);
    echo $html;

json avec python

    import json

    with open('test.json') as content:
        print(json.load(content))

yaml avec python

    import yaml

    with open("test.yaml", 'r') as content:
        print(yaml.load(content))

## api

ensemble normalisé dVe classes, de méthodes ou de fonctions qui sert de façade par laquelle un logiciel offre des services à d'autres logiciels.
API may be for a web-based system, operating system, database system, computer hardware, or software library. 

<http://akareup.alwaysdata.net/aaron-swartz-un-web-programmable.html#ch5>

- requête
- reponse


## web-scraping

extraire données du web (HTTP/S) (automatisation)

- requête
- reponse
- extraire

- avec navigateur
- sans navigateur

(wget, curl, urllib, phantomjs, selenium, beautifulsoup)

    wget --recursive --no-parent --page-requisite -A jpeg,jpg,bmp,gif,png,svg http://parkeddomaingirltombstone.net/
