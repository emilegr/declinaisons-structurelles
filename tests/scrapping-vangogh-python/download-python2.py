#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
from bs4 import BeautifulSoup

html = urllib.urlopen("https://vangoghmuseum.nl/en/search/collection?q=&artist=Vincent%20van%20Gogh&pagesize=42").read()
soup = BeautifulSoup(html)
links = soup.find_all("a", class_="link-teaser")

for link in links:
	print link['href']
	html2 = urllib.urlopen("https://vangoghmuseum.nl"+link['href']).read()
	soup2 = BeautifulSoup(html2)
	link2 = soup2.find_all("a", class_="square")
	#link2 = soup2.find_all("a", class_="rounded-right")
	print link2[0]['href']
	split_text = link['href'].split("/")

	urllib.urlretrieve("https://vangoghmuseum.nl"+link2[0]['href'], split_text[3]+".jpg")





