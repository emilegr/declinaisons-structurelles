# Format de description des données : format de documents (back end). 
Choix de format et du langage d’écriture, voir son incidence conceptuelle sur la gestion ultérieure des informations dans l’édition, structuration des données entre elles (arborescence, sémantique, grille/tableau relationnel, etc.).
Pérennité des données, interopérabilité.

--------------------------------------------------

Organisation d’un workshop type en 3 parties 
1. contexte et récolte de l’existant non exhaustif
2. mise en pratique
3. extrapolation
4. restitution

--------------------------------------------------

# Mots clés
----------
- archivage
- stockage
- obsolescence
- disparition
- conservation
- accessibilité
- classification
- recherche
- filtre
- transmission
- interopérabilité
----------

--------------------------------------------------

Idées :

- À partir des chaussures : comment les décrire ? Quelles classifications ? Quelles relations entre les éléments ?
- Prendre rdv avec : Géraldine, les archives de Valence
- Arpentage


- Là où j’habite :
1. Préparer la projection des cartes des villes / villages d’où les participants sont originaires
2. Dessiner en grand format une carte de France (avec les fleuves et rivières ?)
3. Tour à tour, un participant se place dos à la carte de France et les autres participants doivent placés du mieux qu’ils le peuvent la ville / le village dont le participant est originaire sur le carte de France et sur la carte de la ville / du village, l’habitation
Préparer plusieurs couches successives a ajouter : fleuves/ rivières, relief, autoroutes, résultats d’élections

- Ranger tour à tour une table de travail désordonnée
- https://workbook.conditionaldesign.org/
--------------------------------------------------

## Texte 2

Déclinaisons structurelles (back end) : Vincent Maillard et Émile Greis
Expérimentations et interrogations sur les systèmes d'organisation et de description de contenus. Allers et retours dans la fiction, la spéculation, l'histoire et la technique. Notions d'arborescence, de relations, d'interopérabilité, et d'incidences des formats sur la gestion des contenus. En bref, réflexions communes aux collectionneurs, archivistes, bibliothécaires, philatélistes, lépidoptéristes ou autres énumérophiles.
