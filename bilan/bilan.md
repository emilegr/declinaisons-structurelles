# Bilan du workshop « Déclinaisons structurelles »

## Participants

Porteurs :

- Émile Greis
- Vincent Maillard

Étudiants :

- Chloé Arqué (DG2)
- Marie Madonna (DG3)
- Adèle Pavia (DG3)
- Margot Stevens (DG4)
- Louise Grandcolas (DG3)
- Célia Tremori (DG3)
- Thomas Amico (DG2)
- Ortega Raphael (Dg2)
- Julie Moreau (DG4)
- Terava Jacquemier
- David Pons (DG4)
- Zacharie Houdu(DG2)
- Lorène Tissier (DG2)
- Aurore Demonet (DG3)
- Guillaume Guerin (DG2)
- Quentin B'Chir (DG2)

## Description initiale

> Expérimentations et interrogations sur les systèmes d'organisation et de description de contenus. Allers et retours dans la fiction, la spéculation, l'histoire et la technique. Notions d'arborescence, de relations, d'interopérabilité, et d'incidences des formats sur la gestion des contenus. En bref, réflexions communes aux collectionneurs, archivistes, bibliothécaires, philatélistes, lépidoptéristes ou autres énumérophiles.

## Déroulé

La lancement du workshop a été l'occasion d'introduire des concepts, des idées, des méthodes historiques et prospectives liés à l'organisation de contenus.

La figure du bibliographe belge Paul Otlet (1868-1944) a été notre point de départ. Ses idées concernant un un répertoire bibliographique pour le Mundaneum — centre d'archives constitué par Otlet et Henri La Fontaine — nous ont permis d'introduire la notion de fiche puis de développer les concepts de la classification décimale de Dewey, que l'on retrouvent notamment dans les bibliothèques aujourd'hui. Pour prolonger ces références, nous avons également visionner le film « Toute la mémoire du monde » (Alain Resnais, 1956).

Dans un contexte plus contemporain, nous avons évoqué des méthodes d'accès à des contenus sur le web. Au début du web, les classifications étaient notamment assurées par des sites web qui listaient des contenus par types, par arborescence (art, sport…). Aujourd'hui, le monopole de Google est indéniable. Cela passe par la notion de PageRank. Cette méthode repose sur l'analyse de la structure des hyperliens du web : plus il y a de liens vers une page, plus elle a de valeur. Pour Google, c'est un gage de pertinence et d'objectivité. Ces méthodes sont également appliquées dans la publication scientifique : plus un texte est cité, plus il est reconnu. Nous avons alors décrit l'importance de Elsevier (site de publication scientifique payant) dans ce domaine et la façon dont Sci-Hub (site web créé par Alexandra Elbakyan en 2011) fournit un accès libre à des articles scientifiques.

Pour tisser le lien sur la notion de classification dans les bibliothèques, nous nous sommes intéressés au travail du duo de designer zurichois Astrom/Zimmer pour la bibliothèque de Sitterwerk. Cette bibliothèque a été constituée par le collectionneur Daniel Rohner et ce sans aucun classement. Les designers ont choisi de prolonger ce choix en mettant au point un système de classification dynamique et collaboratif. Le lieu dispose d'une table de travail, d'un scan, de puces RFID et d'un robot géolocalisant l'emplacement de livres. Les visiteurs peuvent prendre des notes, rapprocher des livres et consulter ce que d'autres ont fait avant eux. Le site web catalogue qui en découle est également intéressant : http://www.sitterwerk-katalog.ch/.

Enfin, pour marquer une autre ouverture, nous avons montré le site web « Internet Atlas » (http://internet-atlas.net/) de Louise Drulhe. Dans ce projet, elle développe une approche expérimentale pour créer des modélisations visuelles de l'Internet. Cette approche nous intéressait pour ouvrir le workshop a une dimension moins technique et plus prospective.

Dans une perspective d'ordre plus pratique, nous avons abordé quelques types de formats permettant l'écriture et la lecture de données. Nous avons donc montré comment écrire et lire des formats JSON et CSV.

Le JSON (JavaScript Object Notation) est un format permettant le stockage et l'accès à des données. Les données sont stockées selon une logique de correspondance clé / valeur ce qui garantit un accès aisé par le JavaScript mais aussi une lecture possible pour un utilisateur.

Cette qualité se retrouve dans le CSV : la logique de ce format s'apparente à celle d'un tableau (des colonnes et des lignes). Les données sont stockées sous forme de valeurs séparées par des virgules. On décrit ainsi des lignes et des colonnes.

Ces deux formats permettent de stocker et d'accéder à des données qui peuvent être très nombreuses, de même niveau ou bien avec une logique hiérarchique importante. Par l'intermédiaire d'outils légers, il est posible de les éditer afin de constituer un set de données.

Nous avons également montré des méthodes pour constiturer des collections de données à partir de site web, ce qu'on appelle web scrapping, notamment avec l'outil en ligne commande wget.

Cela a donné lieu à un temps où les étudiants ont découvert comment utiliser leur terminal (installation de programmes et parcours de l'architecture de dossiers).

C'est à ce moment que le serveur de l'école (http://esad-gv.net/designgraphique/) s'est avéré être une source de contenus à explorer. Par exemple, wget a permis de collecter l'ensemble des gifs et d'ainsi de parcourir le serveur d'une manière différente que celle de la succesion des dossiers.


## Description des groupes

Le workshop s'est constitué en quatre groupes, sans mélanges entre classe.

Pour les groupes « Web Insolite » et « Un jour un gif », l'architecture du serveur de l'école (http://esad-gv.net/designgraphique/) a été la source principale d'exploration et de contenu. Ces deux groupes se sont intéressés à la manière de découvrir et de collecter des contenus. Le serveur est apparu comme un espace d'archives sans classement, des chemins permettant l'exploration ainsi qu'un moyen de diffusion de contenus.

Le groupe « Web Insolite »


Pour le groupe de DG2, c'est le sujet « Territoire » qui a été une source de contenu. Les enjeux se sont rapidement portés sur les archives (des photographies notamment) et sur l'étude de différents moyens de les relier à données géographies et temporelles, tout en envisageant des possibilités d'ajout. N'étant pas à l'aise techniquement, les étudiants ont travaillé à partir de schémas et maquettes afin de préciser leurs pensées.

Un groupe n'a pas utilisé des données provenant directement de l'activité de l'école. C'est le groupe formé par Margot Stevens. Son travail a été la collecte et la description de nombreuses interactions avec des éléments web. La mise en forme de cette collecte se situe à la fois dans l'outil pour apprendre et tester des morceaux de code (des snippets comme dans JSFiddle ou CodePen) et dans une bibliothèque analytique de moyens interactifs. L'inventivité réside dans le fait de pouvoir combiner des snippets tout en les cumulant successivement sur une même page. Une question importante a été de définir la manière de décrire le contenu. Le choix s'est porté sur un format JSON pour lister et créer une arborescence.

Explorer le serveur en éclatant la structure hiérarchique : en collectant automatiquement tous les gifs et en retrouvant l'accès dans un deuxième temps. En s'intéressant aux dossiers d'étudiants qui ont arrêtés leur cursus en cours de route (http://esad-gv.net/designgraphique/_archives_etudiants/dossiers_individuels/). 

## Liens

- https://mypads.framapad.org/p/workshop-1-nk2pm7ix
- http://esad-gv.net/designgraphique/_evenements/2018_Espace_editorial/

## Site territoire

- Chloe ARQUE (DG2)
- Thomas AMICO (DG2)
- Terava JACQUEMIER (DG2)
- Lorene TISSIER (DG2)

Comment décrire des données temporels et géographiques ? Se servir d'une nomenclature de fichiers pour décrire le contenu ? Utiliser un fichier de métadonnées.
Que donner à voir pour un visiteur ?

![Capture](SiteTerritoire/captures/22.30.54.png)

![Capture](SiteTerritoire/captures/22.31.07.png)

![Capture](SiteTerritoire/captures/22.31.19.png)

![Capture](SiteTerritoire/captures/22.31.39.png)

![Capture](SiteTerritoire/captures/22.31.53.png)

## Web Insolite

http://esad-gv.net/designgraphique/DG3/Louise_Grandcolas/wk_plateforme_2018/page%20accueil.html
https://mensuel.framapad.org/p/ve4VLgZfmR

- Adele PAVIA (DG3)
- Louise GRANDCOLAS (DG3)
- Celia TREMORI (DG3)

Donner à voir des sites web insolites découverts en explorant le serveur. Notamment en aggrégeant tous les gifs du serveur pour ensuite retrouver 

![Web_Insolite](Web_Insolite/captures/22.36.55.png)

![Web_Insolite](Web_Insolite/captures/22.37.09.png)


## Librairie d'interaction JS

- Margot STEVENS (DG4)

    L'idée de librairie interactive met suite à différente situation que j'ai pu rencontrer en codant.

    Lors de mes productions il m'est souvent arrivé de rechercher une interaction particulière. C'est-à-dire une interaction particulière qui provoque un événement particulier (ex : je clique sur un bouton, le fond de l'écran devient noir). Mais lorsque je cherchais ses exemples je n'avais pas la possibilité de comparer plusieurs interactions et je trouvais souvent le code mélangé avec des informations qui ne m'était pas utile.

    Puis en aidant des camarades je me suis rendu compte que je n'étais pas la seule à avoir le problème.

    J'ai donc imaginé une librairie ou l'utilisateur peut expérimenter plusieurs interactions, les comparer et savoir le contenu du code.

    L'écran se divise en trois parties :

        Une partie ou comporte le choix de l'interaction, divisé en deux sous parties, action et évènement.
        Une partie où l'on retrouve l'écran de visualisation et ou l'on peut expérimenter plusieurs interactions en parallèle
        Une partie ou l'on retrouve le code html, css et javascript de l'interaction.

    Je souhaite par la suite, apporté à cette librairie l'idée de communauté, c'est-à-dire que toute personne qui arrive sur cette page aurait la possibilité d'ajouter une nouvelle interaction au catalogue de la librairie.


![Capture](LibrarieInteractive/ecran_librairie_interactive.png)


## Un jour un gif

- Julie MOREAU (DG4)

Constitution d'une collection de tous les liens de fichiers gif sur le serveur. Envoi random d'un gif par jour.

![Capture](UnJourUnGIf/2019-03-09-16.37.28.png)

![Capture](UnJourUnGIf/2019-03-09-16.37.46.png)


Références :
Workshop "fabriquer des jeux de données en art" :
https://github.com/Humanistica/ArtDesignDH

