# structure organisation hierarchisation

- historique
- utopie
- politique
- technique





## Mundaneum

Paul Otlet
- mundaneum
- CDU
- Dewey
- universalité

Sommaire Mondothèque : https://www.mondotheque.be/wiki/index.php?title=The_radiated_book


## Memex

Vannevar Bush
- 


google intermédiare
xanadu


lines

html
description documents

## Moteurs de recherche web

<https://en.wikipedia.org/wiki/Web_search_engine#History>

début du web

peu de page web

### Annuaires / Répertoires

- Yahoo!, Magellan
- Liste, Arboresence
- Organisé à la main

expensive to build and maintain, slow to improve

### Moteurs de recherches

- Lycos
- mots-clés, fréquence, présence dans titre, importance
- Automatisation

### Google's mission

> Google's mission is to organize the world's information and make it universally accessible and useful.

<https://web.archive.org/web/20160414193720/https://www.google.com/about/>

- activité "principale", "fondatrice" : Google Search, moteur de recherche
- s'agit de présenter des résultats de recherche de contenu web
- questions : quel contenu ? quel ordre ?
- détails du fonctionnement restent opaques, secret
- plusieurs algorithmes mais pillier central : PageRank 
- permit a Google de se distingué des autres moteurs de recherche
- créer avec l'intention d'

> Amener de l'ordre au web

<http://ilpubs.stanford.edu:8090/422/1/1999-66.pdf>

- question de l'"importance", l'"autorité", "valeur", "pertinence" des pages web

> This paper describes PageRank, a method for rating Web pages objectively and
> mechanically, effectively measuring the human interest and attention devoted
> to them.

- noté les pages de manière objectives
- contrairement aux moteurs de recherche précédent
- basé sur l'analyse de la structure des liens du web

> PageRank works by counting the number and quality of links to a page to
> determine a rough estimate of how important the website is. The underlying
> assumption is that more important websites are likely to receive more links
> from other websites.

- "valeur" déterminé par le nombre de liens et la valeur des pages qui ont établis ces liens
- plus seulement en fonction du contenu mais des relations
- parallèle avec la bibliométrie scientifique (lien / citation)
- biais (échanges de citations, etc.) 
- pas seulement PageRank, autres facteurs, affiner les résultats
- contenu des pages
- comportements utilisateurs (géolocalisation, historique, etc.)
- parallèlement autre organisation, classification, catalogage
- celles des comportements et des intentions des utilisateurs
- prédictions



    TODO:
    - "Robots d'exploration" : parcourent, analysent, indexent
    - ordre, classification, proposition, choix
    - économique, politique

    abondance d'information

    simplification, tri, organisation, etc.

    nouvel intermédiaire

    - popularité
    - actualité
    - pertinence


## Ordinateurs : fichiers et répertoires (dossiers)

- comment sont organisées les données ?
- paradigme de fichiers et de répertoires (dossiers) hierarchiques 
  * hierarchie, arborescence, arbre, parent/enfant
  * chemins d'accès, paths

- fichiers
  * contenu
  * metadata 

- répertoire : liste de références vers d'autres fichiers

<https://en.wikipedia.org/wiki/Tree_structure#Representing_trees>




### Xerox Alto / Star


**paradigme fichier / dossier**

- directory / folder
- files / directories
- bureau skeuomorphisme


**filesystems**

https://en.wikipedia.org/wiki/File_system

compatibilité windows / linux / mac


**inode table**

https://en.wikipedia.org/wiki/Inode

    ls -i



           Kid A   
          /        
    Mother         
          \        
           Kid B   

           Kid A   
          /        
    Father         
          \        
           Kid B

Francis Hunger, *deep love algorithms*

    références/
      typographie/
      signalétique/

    belle-enseigne.jpeg ?


200 points of lights

## *Sitterwerk*

- description lieu / concept 1 phrase

### *Art Library*

- Collectionneur suisse Daniel Rohner
- Don collection livres d'art (catalogue d'expo, monographie, etc.)

> The Art Library in the Sitterwerk is a reference library with around 25,000
> volumes on art, architecture, material science, and casting technology. The
> holdings go back to a gift by the book collector Daniel Rohner, but are
> continuously being expanded.

<http://www.sitterwerk-katalog.ch/intro>

- système de catalogage / non-catalogage
- puces RFID / shelf-scanning

> The inventory of around 19,000 books is accessible to the public as a
> reference library. This inventory is currently being recorded and catalogued.
> At this time, 16,000 books have already been processed from a library-science
> perspective and can be called up on the Sitterwerk catalogue. In a pilot
> project, an RFID reading device makes it possible to find the books in the Art
> Library. This allows for a continuous inventory and thus a dynamic order. The
> Library can be adapted to the user, who can bring together subject-specific or
> also associatively related books in the shelves. Serendipitous discoveries
> thus become possible in the Art Library: when people search for specific
> books, they find other books that they had not been looking for but that,
> nevertheless, lie within the scope of their interests. The compilations of
> books are saved in the database so that this principle also leads to
> unexpected and yet specific discoveries in the digital catalogue.

<http://www.sitterwerk.ch/en/art-library/dynamic-order.html>

### *Katalog*

- http://www.sitterwerk-katalog.ch/

## *Werkbank*, Astrom/Zimmer, 2014

Astrom / Zimmer

az sitterwerk
modif
https://vimeo.com/157990864


google intermédiare
push









### cfc

structure of computer universe
files and hierarchical directory (folder)
inter-connecté, non-linéaire, chevauchant
représentation manipulation ?

punch card with a name
census 1888 
metadata ?

file system
- NTFS windows
- HFS+ macintosh

incompatibilité


2 tables
- inodes table
- paths table

- out of inodes
- losing files to deep

one way shortcut
not moving


Xerox PARC
modern computer instead the geeky one
- magic
- mouse
- drop down 
- GUI

or PUI PARC User Interface

Xerox Alto
WYSIWYG
Xerox Star

took away the right to program


immiter le passé
construction appellée métaphore
tout doit ressembler au passé
sans penser que l'on créer de nouvelle construction



database mess
any systematic attemps to keep track of things
gmail sent mails
:w

60s 70s computer as database
IBM IMS
IBM sage keeping track of incoming soviet bombers


imitation of the past
ancient tradition
modernin simplification
replicating the past

hierarchical directories

innapropriate
secretarial functions
application prisons

