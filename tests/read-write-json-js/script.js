function get() {
    fetch('data.json')
        .then(function(response) {
            return response.json();
        })
        .then(function(json) {
            console.log(json);
            var table = document.getElementsByTagName('table')[0];
            while (table.hasChildNodes()) {
                table.removeChild(table.lastChild);
            }
            for (var i = 0; i < json.length; i++) {
                var tr = document.createElement('tr');
                var td = document.createElement('td');
                var text = document.createTextNode(json[i]);
                td.appendChild(text);
                tr.appendChild(td);
                table.appendChild(tr);
            }
        })
}

function post() {
    var value = document.getElementsByTagName('input')[0].value;
    if (!value) {
        alert('wrong input');
        return
    } 
    fetch('post.php', { method: 'POST', body: value })
        .then(function(response) { 
            return response.text();
        })
        .then(function(text) {
            alert(text);
            document.getElementsByTagName('input')[0].value = '';
        })
        .then(function() {
            get();
        })
}

document.getElementsByTagName('button')[0].addEventListener("click", post);

get();


