#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib.request
from bs4 import BeautifulSoup

html = urllib.request.urlopen("https://vangoghmuseum.nl/en/search/collection?q=&artist=Vincent%20van%20Gogh&pagesize=42").read()
soup = BeautifulSoup(html)
links = soup.find_all("a", class_="link-teaser")

for link in links:
    print(link['href'])
    html2 = urllib.request.urlopen("https://vangoghmuseum.nl"+link['href']).read()
    soup2 = BeautifulSoup(html2)
    #link2 = soup2.find_all("a", class_="rounded-left")
    link2 = soup2.find_all("a", class_="button dark-hover square")
    #link2 = soup2.find_all("a", class_="rounded-right")
    print(link2[0]['href'])
    split_text = link['href'].split("/")
    print(split_text[3])
    urllib.request.urlretrieve("https://vangoghmuseum.nl"+link2[0]['href'], 'data/' + split_text[3]+".jpg")
